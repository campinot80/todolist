import React from 'react';

function Todo({ todo, toggleTodo }) {

    function handleTodoClick() {
        toggleTodo(todo.id)
    }

    return ( 
        <div className='todo-eintrag'>
            <label className='kranke-sache'>
                <input className='m-2' type="checkbox" checked={todo.complete} onChange={handleTodoClick}></input>
                {todo.name}
            </label>
        </div>
     );
}

export default Todo;