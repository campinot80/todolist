import React, { useState, useRef, useEffect } from 'react';
import TodoList from './TodoList';
import uuid from 'react-uuid';
import NavBar from './navbar';

const LOCAL_STORRAGE_KEY = 'todoApp.todos'

function App() {
  const [todos, setTodos] = useState([]);
  const todoNameRef = useRef();

  // USE-EFFECT GREIFT AUF LOCALSTORAGE ZU UND AKTUALISIERT "todos"
  useEffect(() => {
    const storedTodos = JSON.parse(localStorage.getItem(LOCAL_STORRAGE_KEY))
    if (storedTodos) setTodos(storedTodos)
  }, [])

  // USE-EFFECT SPEICHERT "todos" IN "LOCAL_STORRAGE_KEY"-const 
  useEffect(() => {
    localStorage.setItem(LOCAL_STORRAGE_KEY, JSON.stringify(todos))
  }, [todos])

  // TODOS_CHECKBOX_TOGGLE
  function toggleTodo(id) {
    const newTodos = [...todos]
    const todo = newTodos.find(todo => todo.id === id)
    todo.complete = !todo.complete
    setTodos(newTodos)
  } 

  // ADD-BUTTON EventHandler
  function handleAddTodo(e) {
    const name = todoNameRef.current.value
    if (name === '') return 
    setTodos(prevTodos => {
      return [...prevTodos, {id: uuid(), name: name, complete: false}]
    });
    todoNameRef.current.value = null;
  }

  // CLEAR-BUTTON EventHandler
  function handleClearTodos() {
    const newTodos = todos.filter(todo => !todo.complete)
    setTodos(newTodos)
  }

  return (
    <>
      <NavBar />
      <div className='d-flex flex-column p-4'>
        <div className='d-flex flex-row eintrag-control'>
          <div className='control-input'>
            <p className=''>NEUER EINTRAG:</p>
            <input /*onChange={handleAddTodo}*/ ref={todoNameRef} className='m-2' type='text' />
          </div>
          <div className='control-btns'>
            <button className='btn btn-warning btn-sm p-2 m-2' onClick={handleAddTodo}>Add Todo</button>
            <button className='btn btn-danger btn-sm p-2 m-2' onClick={handleClearTodos}>Clear Todo's</button>
          </div>
        </div>
        <TodoList todos={todos} toggleTodo={toggleTodo} />
        <div>{todos.filter(todo => !todo.complete).length} left to do</div>
      </div>
    </>
  )
}

export default App;
