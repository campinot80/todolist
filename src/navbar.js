import React from 'react';

function NavBar() {
    return (  
        <nav className="navbar navbar-expand-md navbar-light bg-dark nav-height">
            <div className="container">
                <a className="navbar-brand text-white nav-h1" href="#">To-Do-List [react-app]</a>
                <a className="text-white p-2" target="_blank" href='https://www.youtube.com/watch?v=Ke90Tje7VS0&t=7579s'>CLICK ME</a>
            </div>
        </nav>
    );
}

export default NavBar;
